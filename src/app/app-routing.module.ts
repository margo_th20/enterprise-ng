import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentComponent } from './department/department.component';
import { EnterpriseComponent } from './enterprise/enterprise.component';
import { Enterprise } from './enterprise/model/enterprise';

const routes: Routes = [
  {path: 'enterprise', component:EnterpriseComponent},
  {path: 'department/:id', component:DepartmentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
