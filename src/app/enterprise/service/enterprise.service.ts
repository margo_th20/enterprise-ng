import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Enterprise } from '../model/enterprise';

@Injectable({
  providedIn: 'root'
})
export class EnterpriseService {

  private baseUrl = "http://localhost:8080/api/enterprise/"
  constructor(private http: HttpClient) { }

  findAllEnterprise(): Observable<Enterprise[]> {
    return this.http.get<Enterprise[]>(`${this.baseUrl}findAllEnterprise`)
  }
}
