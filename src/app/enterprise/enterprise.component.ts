import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Enterprise } from './model/enterprise';
import { EnterpriseService } from './service/enterprise.service';

@Component({
  selector: 'app-enterprise',
  templateUrl: './enterprise.component.html',
  styleUrls: ['./enterprise.component.css']
})
export class EnterpriseComponent implements OnInit {


  
  enterprises: Enterprise[] = [];
  idEnterprise: any;
  constructor(private enterpriseService: EnterpriseService, private router: Router) { }

  ngOnInit(): void {
    this.enterpriseService.findAllEnterprise().subscribe((data: Enterprise[]) => {
      this.enterprises = data;
    });
  }

  viewDepartments(idEnterprise: number) {
    console.log(idEnterprise)
    this.router.navigate(["department/" + idEnterprise]);
  }

}
