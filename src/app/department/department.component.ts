import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Department } from './model/department';
import { DepartmentService } from './service/department.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {

  departments: Department[] = [];
  idEnterprise: any;
  constructor(private departmentService: DepartmentService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(param => this.idEnterprise = param.get("id"));
    this.departmentService.findAllDepartment(this.idEnterprise).subscribe((data: Department[]) => {
      this.departments = data;
    });
  }

  viewDepartments() {
    this.router.navigate(["department"]);
  }

}
