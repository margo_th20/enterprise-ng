import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Department } from '../model/department';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  private baseUrl = "http://localhost:8080/api/department/"
  constructor(private http: HttpClient) { }

  findAllDepartment(idEnterprise: number): Observable<Department[]> {
    return this.http.get<Department[]>(`${this.baseUrl}findAllDepartments/` + idEnterprise);
  }
}
