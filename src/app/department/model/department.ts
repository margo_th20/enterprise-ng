import { Injectable } from "@angular/core";

@Injectable()
export class Department {
    id: number = 0;
    description: String = "";
    name: String = "";
    phone: String = "";
    idEnterprise: number = 0;
}
