import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'enterprise-ng';

  constructor(private router: Router){}

  viewEnterprises() {
    this.router.navigate(["enterprise"]);
  }

  viewDepartments() {
    this.router.navigate(["department"]);
  }
}
